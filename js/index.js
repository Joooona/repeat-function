/*
Toistofunktio mielivaltaiselle funktiolle. 

Funktio Toista(a,b,c) ottaa argumenteikseen toistojen määrän a, toistettavan funktion b ja toiston nopeuden c. Funktio helpottaa ja monipuolistaa funktioiden setInterval() tai setTimeout() käyttöä. 
*/

//Esimerkkifunktio. Tähän on mahdollista määritellä toistettava funktio. Funktiossa on mahdollista käyttää tietoa paraikaa käynnissä olevan toiston numerosta. Tähän voi viitata funktiolla laskija.tila(), kuten alla.
function aaa(){
document.getElementById("kk").innerHTML=laskija.tila();
}

Toista(999,aaa,5);












//Toistofunktion rakenne
function Toista(a,b,c){

//Kellotuskoneisto. Mahdollistaa kierroksen lukumäärän laskemisen.
laskija = (function(){
var tila = 0;
var lisäys = function(){
tila++;
};
lisäys.tila = function(){
return tila;
}
return lisäys;
})();

//Paikanpitäjä. Sijoittaa funktion Toista() funktion oikealle paikalle. Toistettava funktio b() on sijaittava ylläolevan kellotuskoneiston jälkeen.
b();

//Itse periodifunktion suoritus.
periodic(a,b);

//Periodifunktion kuvaus.
function periodic(kesto,funktio){

//Runko.
function actions(){
laskija();
funktio();
lopetus();

//Lopetusfunktion kuvaus.
function lopetus(){
var ohje;
if (kesto == laskija.tila()){
ohje = "lopeta";
}
if(ohje != "lopeta"){
init();
}
}
}

//Toistaja
function init(){
setTimeout(actions,c);
}

actions();

}

}